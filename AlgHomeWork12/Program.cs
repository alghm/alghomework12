﻿using System.Text;

public class Program
{
    public static void Main(string[] args)
    {
        var hashTable = new HashTable();

        for (int i = 0; i < 1000; i++)
        {   
            hashTable.Insert(i.ToString(), $"Value = {i}");
        }


        int index = -1;
        var indents = new StringBuilder();
        foreach (var item in hashTable.Nodes)
        {
            index++;
            if (item == null)
            {
                continue;
            }

            var count = 1;
            indents.AppendLine(index.ToString());
            indents.AppendLine(new string(' ', count) + "->" + item.Key);

            var nextNode = item.Next;
            while (nextNode != null)
            {
                count++;
                indents.AppendLine(new string(' ', count) + "->" + nextNode.Key);

                nextNode = nextNode.Next;
            }
        }
        Console.WriteLine(indents);

        Console.WriteLine();
    }
}

public class HashTable
{
    private const int NodesSize = 128;
    public Node[] Nodes;

    public HashTable()
    {
        Nodes = new Node[NodesSize];
    }

    private int Hash (string key)
    {
        int hash = 0;
        foreach (char c in key)
        {
            hash += c;
        }
        return hash % NodesSize;
    }

    public void Insert(string key, string value)
    {
        int index = Hash(key);
        Node newNode = new Node(key, value);
        if (Nodes[index] == null)
        {
            Nodes[index] = newNode;
        }
        else
        {
            Node current = Nodes[index];
            while (current.Next != null)
            {
                current = current.Next;
            }
            current.Next = newNode;
        }
    }

    public string Search(string key)
    {
        int index = Hash(key);
        Node current = Nodes[index];
        while (current != null)
        {
            if (current.Key == key)
            {
                return current.Value;
            }
            current = current.Next;
        }
        return null;
    }

    public void Delete(string key)
    {
        int index = Hash(key);
        Node current = Nodes[index];
        Node previous = null;
        while (current != null)
        {
            if (current.Key == key)
            {
                if (previous == null)
                {
                    Nodes[index] = current.Next;
                }
                else
                {
                    previous.Next = current.Next;
                }
                return;
            }
            previous = current;
            current = current.Next;
        }
    }
}

public class Node
{
    public string Key { get; set; }
    public string Value { get; set; }
    public Node Next { get; set; }

    public Node(string key, string value)
    {
        Key = key;
        Value = value;
    }
}